This directory contains tasks which are needed for the build process.

Build these tasks (in case of an update) with

./build.sh compile-build-tasks

Related:

  http://antinstaller.sourceforge.net/
