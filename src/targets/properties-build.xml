<?xml version="1.0"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<!-- $Id: properties-build.xml 1076695 2011-03-03 17:31:55Z rainer $ -->

<project name="lenya-properties">

  <description>
    Properties Targets
  </description>

  <!-- Set up some global properties -->
  <!--
  Give user a chance to override without editing build.properties
  (local.build.properties takes precedence)
  -->
  <property file="local.build.properties"/>
  <property file="build.properties"/>

  <condition property="local.properties.in.sync">
    <equals arg1="${build.properties.version}" arg2="594644"/>
  </condition>

  <fail unless="local.properties.in.sync" message="It seems that your local.build.properties is NOT in sync with build.properties! The file build.properties has probably been modified by some Apache Lenya developer. Re-copy build.properties to local.build.properties and re-set your local properties to get the two files back in sync. Please apologize any inconvenience."/>

  <!-- cocoon build properties directory -->
  <property name="src.cocoon.properties.dir" value="src/cocoon"/>

  <!-- Project version and name -->
  <property name="version" value="2.0.4"/>
  <property name="lenya.revision" value="xxxxxx"/>
  <property name="fullname" value="${ant.project.name}-${version}"/>
  <property name="distname" value="apache-${ant.project.name}-${version}"/>
  <property name="Name"     value="Apache Lenya"/>
  <property name="year"     value="1999-2010"/>

  <property name="debug"          value="on"/>
  <property name="optimize"       value="on"/>
  <property name="deprecation"    value="off"/>
  <property name="nowarn"         value="off"/>
  <!-- Detecting the current jvm -->
  <condition property="target.vm" value="${ant.java.version}">
    <not>
      <or>
        <equals arg1="1.1" arg2="${ant.java.version}"/>
        <equals arg1="1.2" arg2="${ant.java.version}"/>
        <equals arg1="1.3" arg2="${ant.java.version}"/>
      </or>
    </not>
  </condition>
  <fail unless="target.vm">Please use a JVM 1.4 or superior to compile lenya ${version}</fail>
  
  <!--
  The minimal supported Java version. The class files we ship with Lenya (e.g., build tasks)
  are compiled for this version.
  -->
  <property name="target.vm.minimal" value="1.4"/>

  <property name="packages"      value="*.*"/>

  <!-- cocoon endorsed libraries directory -->
  <property name="cocoon.endorsed.lib.dir" value="${cocoon.src.dir}/lib/endorsed"/>

  <!-- The libraries we use and which are not already included in the cocoon package -->
  <property name="lib.dir" value="lib"/>
  <property name="tools.lib.dir" value="tools/lib"/>
  <property name="endorsed.lib.dir" value="lib/endorsed"/>
  <!-- All compilation and preparation takes place in build.root -->
  <property name="build.root" value="build"/>
  <!-- Destination for the compliation of our own classes in the "compile" target -->
  <property name="build.dir" value="${build.root}/${ant.project.name}"/>
  <!-- Destination for the filtered source files  -->
  <property name="build.java.src" value="${build.dir}/java/src"/>
  <property name="build.java.test" value="${build.dir}/java/test"/>
  
  <!-- Destination for temporary files during the build  -->
  <property name="build.temp" value="${build.dir}/temp"/>
  <property name="build.patch" value="${build.temp}/patch"/>
  <!-- This is where we assemble the webapp directory in the "webapp" task -->
  <property name="build.webapp" value="${build.dir}/webapp"/>
  <!-- This is where we do the tests -->
  <property name="build.test" value="${build.root}/test"/>
  <!-- All packaging for distribution takes place in dist.root -->
  <property name="dist.root" value="dist"/>
  <!-- The toplevel directory for the final end-user distribution -->
  <property name="dist.bin.dir" value="${dist.root}/${distname}-bin"/>
  <!-- The javadoc destination directory -->
  <property name="dist.bin.javadocs" value="${build.webapp}/site/apidocs"/>
  <!-- The filename of the final end-user war package -->
  <property name="dist.bin.warfile" value="${dist.bin.dir}/${ant.project.name}.war"/>
  <!-- The toplevel directory for the final developer source distribution -->
  <property name="dist.src.dir" value="${dist.root}/${distname}-src"/>
  <!--
  The files that should be copied into the dist
  directory and packed up as tar.gz file, along with the .war file
  -->
  <property name="src.dist.dir" value="./src/dist"/>
  <!--
  The files that should be copied into the source dist
  directory and packed up as tar.gz file, along with the sources etc.
  -->
  <property name="src.dist-src.dir" value="./src/dist-src"/>
  
  <!-- Our own source code tree is here -->
  <property name="src.java.api.dir" value="./src/java"/>
  <property name="src.java.impl.dir" value="./src/impl/java"/>
  
  <!-- This is where the source of the tests are located -->
  <property name="src.test.api.dir" value="src/test"/>
  <property name="src.test.impl.dir" value="src/impl/test"/>
  
  <!-- Publication-specific entities are here -->
  <property name="src.entities.dir" value="resources/entities"/>
  <property name="build.entities.dir" value="WEB-INF/entities"/>
  <!--
  Our own webapp resources to be merged with the
  contents of the cocoon webapp are here
  -->
  <property name="src.webapp.dir" value="src/webapp"/>
  <property name="tools.tasks.dest" value="tools/anttasks/build/classes"/>
  <property name="src.resource.dir"  value="src/resources"/>
  <!-- hsqldb database config files directory -->
  <property name="src.database.dir" value="src/webapp/WEB-INF/db"/>

  <!-- Set up classpath -->
  <path id="classpath">
    <fileset dir="${lib.dir}">
      <include name="*.jar"/>
    </fileset>
    <fileset dir="${build.dir}/lib">
      <include name="*.jar"/>
    </fileset>
    <fileset dir="${cocoon.webapp.dir}/WEB-INF/lib">
      <include name="*.jar"/>
    </fileset>
    <fileset dir="tools/jetty/lib">
      <include name="servlet-*.jar"/>
    </fileset>
  </path>

  <target name="cocoon-init-tasks">
    <ant dir="${cocoon.src.dir}" target="init-tasks" inheritall="false"/>
  </target>

  <!-- Custom Ant tasks used for building Lenya -->
  <target name="init-tasks" depends="cocoon-init-tasks">
    <taskdef name="copyJavaSources" classpath="${tools.tasks.dest}" classname="org.apache.lenya.cms.ant.CopyJavaSourcesTask"/>
    <taskdef name="bulkCopy"        classpath="${tools.tasks.dest}" classname="org.apache.lenya.cms.ant.BulkCopyTask"/>
    <taskdef name="copyPubs"        classpath="${tools.tasks.dest}" classname="org.apache.lenya.cms.ant.CopyPublicationsTask"/>
    <taskdef name="xpatch"          classpath="${cocoon.src.dir}/tools/anttasks" classname="XConfToolTask" />
    <taskdef name="generateModuleList" classpath="${tools.tasks.dest}" classname="org.apache.lenya.cms.ant.GenerateModuleList" />
    <taskdef name="jing" classname="com.thaiopensource.relaxng.util.JingTask"/>
  </target>

  <!-- Compile tasks -->
  <target name="compile-build-tasks" description="Target to compile tasks needed during build">
    <javac srcdir="tools/anttasks/src"
           destdir="tools/anttasks/build/classes"
           debug="${debug}"
           optimize="{optimize}"
           deprecation="${deprecation}"
           target="${target.vm.minimal}"
           nowarn="${nowarn}"
           source="${target.vm.minimal}"/>
	   <!--
           classpathref="classpath"
	   -->
  </target>

  <!-- Compile Configure Command Line Tool -->
  <target name="compile-configure" description="Compile configure command line tool">

    <delete dir="tools/configure/build"/>

    <mkdir dir="tools/configure/build"/>
    <mkdir dir="tools/configure/build/classes"/>
    <mkdir dir="tools/configure/build/lib"/>
  	
    <javac srcdir="tools/configure/src/java"
           destdir="tools/configure/build/classes"
           debug="${debug}"
           optimize="{optimize}"
           deprecation="${deprecation}"
           target="${target.vm}"
           nowarn="${nowarn}"
           source="${target.vm}"/>
	   <!--
           classpathref="classpath"
	   -->


    <jar destfile="tools/configure/build/lib/apache-lenya-configure-core-${version}-r${lenya.revision}.jar"
         basedir="tools/configure/build/classes" 
         includes="org/apache/lenya/config/core/**"/>
    <jar destfile="tools/configure/build/lib/apache-lenya-configure-impl-${version}-r${lenya.revision}.jar"
         basedir="tools/configure/build/classes" 
         includes="org/apache/lenya/config/impl/**"/>

    <delete>
      <fileset dir="tools/configure/lib" includes="*.jar"/>
    </delete>

    <copy file="tools/configure/build/lib/apache-lenya-configure-core-${version}-r${lenya.revision}.jar"
          tofile="tools/configure/lib/apache-lenya-configure-core-${version}-r${lenya.revision}.jar"/>
    <copy file="tools/configure/build/lib/apache-lenya-configure-impl-${version}-r${lenya.revision}.jar"
          tofile="tools/configure/lib/apache-lenya-configure-impl-${version}-r${lenya.revision}.jar"/>
          
  </target>

</project>

