<?xml version="1.0" encoding="UTF-8"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<!-- $Id: global-sitemap.xmap 546840 2007-06-13 12:38:45Z andreas $ -->
    
<map:sitemap xmlns:map="http://apache.org/cocoon/sitemap/1.0">

  <map:pipelines>
   
    <map:pipeline>
      <map:match pattern="dojo/**">
        <map:read src="resource://org/apache/cocoon/dojo/resources/{1}"/>
      </map:match>
    </map:pipeline> 
   
    <map:pipeline internal-only="true">
      
      <!-- Generate a meta-stylesheet that adds the Lenya GUI menu to pages -->
      <!-- {pub}/{area}/lenyaGui.xsl -->
      <map:match pattern="*/*/lenyaGui.xsl">
        <!-- no menu for revisions -->
        <map:match type="request-parameter" pattern="lenya.revision">
          <map:generate src="fallback://lenya/xslt/util/identity.xsl"/>
          <map:serialize type="xml"/>
        </map:match>
        <!-- for everything else: --> 
        <map:generate src="cocoon:/{1}/lenyaMenuBar.xml"/>
        <map:transform type="menufilter"/>
        <!-- Disable menu items according to usecase policies -->
        <map:transform type="usecasemenu"/>
        <map:transform src="fallback://lenya/modules/menubar/xslt/menubar2xhtml.xsl">
          <map:parameter name="publicationid" value="{1}"/>
          <map:parameter name="area" value="{2}"/>
          <map:parameter name="documenturl" value="{page-envelope:document-url}"/>
          <map:parameter name="documentid" value="{page-envelope:document-uuid}"/>
          <map:parameter name="userid" value="{access-control:user-id}"/>
          <map:parameter name="servertime" value="{date-i18n:currentDate}"/>
          <map:parameter name="workflowstate" value="{workflow:state}"/>
          <map:parameter name="islive" value="{workflow:variable.is_live}"/>
          <map:parameter name="usecase" value="{request-param:lenya.usecase}"/>
          <map:parameter name="tabGroup" value="{usecase:tabGroup:{request-param:lenya.usecase}}"/>
          <map:parameter name="newMessages" value="{inbox:newMessageCount}"/>
        </map:transform>
        <map:transform type="i18n">
          <map:parameter name="locale" value="{request:locale}"/>
        </map:transform>
        <!-- Generate the meta-stylesheet -->
        <map:transform src="fallback://lenya/modules/menubar/xslt/menu2xslt.xsl">
          <map:parameter name="publicationid" value="{1}"/>
          <map:parameter name="area" value="{2}"/>
          <map:parameter name="documenturl" value="{page-envelope:document-url}"/>
        </map:transform>
        <map:serialize type="xml"/>
      </map:match>
      
      
      <!-- collect the menu items for the publication and all modules used in the publication -->
      <!-- {pubId}/lenyaMenuBar.xml -->
      <map:match pattern="*/lenyaMenuBar.xml">
        <map:generate src="aggregate-fallback://config/publication.xml"/>
        <map:transform src="fallback://lenya/modules/menubar/xslt/modules2include.xsl">
          <map:parameter name="pub" value="{1}"/>
        </map:transform>
        <map:transform type="include"/>
        <map:transform src="fallback://lenya/modules/menubar/xslt/merge-menus.xsl"/>
        <map:serialize type="xml"/>
      </map:match>
      
      <!--
        FIXME: The following pipelines should be replaced by a direct inclusion of the menu XML,
        so that the XML can be cached. This is not possible yet because we support dynamic menus
        for backwards compatibility.
      -->
      
      <!-- query menu items from modules (used by the xinclude above) -->
      <!-- menu-xml/module/{area}/{module-id}.xml -->
      <map:match pattern="menu-xml/module/*.xml">
        <map:select type="resource-exists">
          <map:when test="fallback://lenya/modules/{1}/config/menu.xml">
            <map:generate src="fallback://lenya/modules/{1}/config/menu.xml"/>
            <map:serialize type="xml"/>
          </map:when>
          <!-- @deprecated: Mount menus.xmap for backwards-compatibility reasons -->
          <map:when test="fallback://lenya/modules/{1}/menus.xmap">
            <map:mount uri-prefix="menu-xml/module/" src="{fallback:lenya/modules/{1}/menus.xmap}" check-reload="true" reload-method="synchron" pass-through="true"/>
          </map:when>
          <map:otherwise>
            <map:generate src="resources/content/emptyMenu.xml"/>
            <map:serialize type="xml"/>
          </map:otherwise>
        </map:select>
      </map:match>
      
      <!-- include publication-specific menus -->
      <!-- menu-xml/pub/{publication-id}/... -->
      <map:match pattern="menu-xml/pub/*.xml" internal-only="true">
        <map:select type="resource-exists">
          <map:when test="fallback://config/menu.xml">
            <map:generate src="fallback://config/menu.xml"/>
            <map:serialize type="xml"/>
          </map:when>
          <!-- @deprecated: Mount menus.xmap for backwards-compatibility reasons -->
          <map:otherwise>
            <map:mount uri-prefix="menu-xml/pub/{1}" src="{fallback:menus.xmap}" check-reload="true" reload-method="synchron" pass-through="true"/>
          </map:otherwise>
        </map:select>
      </map:match>
      
    </map:pipeline>
   

    <map:pipeline internal-only="true">
      
      <map:match pattern="addLenyaGui.xsl">
        <map:select type="parameter">
          <map:parameter name="parameter-selector-test" value="{page-envelope:area}"/>
          <map:when test="live">
            <map:generate src="context://lenya/xslt/util/identity.xsl"/>
          </map:when>
          <map:otherwise>
            <map:generate src="fallback://lenya/modules/menubar/xslt/addLenyaGui.xsl"/>
          </map:otherwise>
        </map:select>
        <map:serialize type="xml"/>
      </map:match>
      
      <!-- AJAX menubar -->
      <!-- ajaxmenu/{1:pubId}/{2:area}/{3:...} -->
      <map:match pattern="ajaxmenu/*/*/**">
        <map:generate src="xslt/emptyMenu.xml"/>
        <map:transform src="fallback://lenya/xslt/menu/menubar2xhtml.xsl">
          <map:parameter name="publicationid" value="{1}"/>
          <map:parameter name="area" value="{2}"/>
          <map:parameter name="documenturl" value="{page-envelope:document-url}"/>
          <map:parameter name="documentid" value="{page-envelope:document-uuid}"/>
          <map:parameter name="userid" value="{access-control:user-id}"/>
          <map:parameter name="servertime" value="{date-i18n:currentDate}"/>
          <map:parameter name="workflowstate" value="{workflow:state}"/>
          <map:parameter name="islive" value="{workflow:variable.is_live}"/>
          <map:parameter name="usecase" value="{request-param:lenya.usecase}"/>
          <map:parameter name="tabGroup" value="{usecase:tabGroup:{request-param:lenya.usecase}}"/>
          <map:parameter name="newMessages" value="{inbox:newMessageCount}"/>
        </map:transform>
        <map:transform src="xslt/menu2xslt-ajax.xsl">
          <map:parameter name="areaPrefix" value="{proxy:/{1}/{2}}"/>
          <map:parameter name="publicationid" value="{1}"/>
          <map:parameter name="area" value="{2}"/>
          <map:parameter name="documenturl" value="{page-envelope:document-url}"/>
          <map:parameter name="queryString" value="{request:queryString}"/>
        </map:transform>
        <map:transform type="i18n">
          <map:parameter name="locale" value="{request:locale}"/>
        </map:transform>
        <map:serialize type="xml"/>
      </map:match>
      
      
      <!-- menu-xml/modules -->
      <!-- menu-xml/modules/{pub-id}/{area}.xml -->
      <map:match pattern="menu-xml/modules/*/*.xml">
        <map:generate src="aggregate-fallback://config/publication.xml"/>
        <map:transform src="fallback://lenya/xslt/modules/modules2xinclude.xsl">
          <map:parameter name="area" value="{2}"/>
        </map:transform>
        <map:transform type="xinclude"/>
        <map:serialize type="xml"/>
      </map:match>

      <!-- menu-xml/module/{area}/{module-id}.xml -->
      <map:match pattern="menu-xml/module/*/*.xml">
        <map:select type="resource-exists">
          <map:when test="fallback://lenya/modules/{2}/menus.xmap">
            <map:mount uri-prefix="menu-xml/module/{1}/" src="{fallback:lenya/modules/{2}/menus.xmap}" check-reload="true" reload-method="synchron" pass-through="true"/>
          </map:when>
          <map:otherwise>
            <map:generate type="serverpages" src="fallback://lenya/content/menus/live.xsp"/>
            <map:serialize type="xml"/>
          </map:otherwise>
        </map:select>
      </map:match>
      
      <!-- menu-xml/{publication-id}/admin/... -->
      <map:match pattern="menu-xml/*/admin/**" internal-only="true">
        <map:generate type="serverpages" src="lenya/content/menus/admin.xsp"/>
        <map:serialize type="xml"/>
      </map:match>
      
      <!-- menu-xml/{publication-id}/... -->
      <map:match pattern="menu-xml/*/**" internal-only="true">
        <map:mount uri-prefix="menu-xml/{1}/" src="{fallback:menus.xmap}" check-reload="true" reload-method="synchron" pass-through="true"/>
      </map:match>
      
    </map:pipeline>
    
   <map:pipeline>
     
     <!-- Default Lenya menubar -->
     <!-- {1:pubId}/{2:area}/{3:...} -->
     <map:match pattern="*/*/**">
       
       <map:aggregate element="menu" ns="http://apache.org/cocoon/lenya/menubar/1.0">
         <map:part src="cocoon:/menu-xml/{1}/{2}/{3}" strip-root="true"/>
         <map:part src="cocoon:/menu-xml/modules/{1}/{2}.xml" strip-root="true"/>
       </map:aggregate>
       
       <map:transform src="fallback://lenya/xslt/menu/merge-menus.xsl"/>
       
       
       <map:transform src="xslt/selectSingleMenu.xsl">
         <map:parameter name="menu" value="{request-param:lenya.menu}"/>
       </map:transform>
       
       <map:transform src="fallback://lenya/xslt/menu/filter-menu.xsl">
         <map:parameter name="tabGroup" value="{usecase:tabGroup:{request-param:lenya.usecase}}"/>
       </map:transform>
       
       <!-- Disable menu items according to usecase policies -->
       <map:transform type="usecasemenu"/>
       
       <map:transform src="fallback://lenya/xslt/menu/menu2xhtml.xsl">
         <map:parameter name="publicationid" value="{1}"/>
         <map:parameter name="area" value="{2}"/>
         <map:parameter name="documenturl" value="{page-envelope:document-url}"/>
         <map:parameter name="documentid" value="{page-envelope:document-uuid}"/>
         <map:parameter name="userid" value="{access-control:user-id}"/>
         <map:parameter name="servertime" value="{date-i18n:currentDate}"/>
         <map:parameter name="workflowstate" value="{workflow:state}"/>
         <map:parameter name="islive" value="{workflow:variable.is_live}"/>
         <map:parameter name="usecase" value="{request-param:lenya.usecase}"/>
         <map:parameter name="tabGroup" value="{usecase:tabGroup:{request-param:lenya.usecase}}"/>
         <map:parameter name="newMessages" value="{inbox:newMessageCount}"/>
         <map:parameter name="position" value="{request-param:lenya.menu}"/>
       </map:transform>
       
       <map:transform type="i18n">
         <map:parameter name="locale" value="{request:locale}"/>
       </map:transform>
       <map:transform src="xslt/convertMessages.xsl"/>
       <map:transform src="fallback://lenya/modules/prettyprinting/xslt/xml2nicexml.xsl"/>
       <map:serialize type="xml"/>
     </map:match>
     
   </map:pipeline>
   
 </map:pipelines>

</map:sitemap>
