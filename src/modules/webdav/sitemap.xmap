<?xml version="1.0"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<map:sitemap xmlns:map="http://apache.org/cocoon/sitemap/1.0">

<!-- =========================== Components ================================ -->

  <map:components>
    <map:matchers default="wildcard">
      <map:matcher name="destination" src="org.apache.cocoon.matching.WildcardHeaderMatcher">
        <header-name>Destination</header-name>
      </map:matcher>
    </map:matchers>

    <map:readers default="resource">
       <map:reader logger="sitemap.reader.requestreader" name="request" pool-max="16" src="org.apache.cocoon.reading.RequestReader"/>
    </map:readers>
  
    <map:serializers default="xhtml"> 
      <map:serializer name="xml-get" src="org.apache.cocoon.serialization.XMLSerializer" mime-type="text/xml">
        <encoding>utf-8</encoding>
        <omit-xml-declaration>yes</omit-xml-declaration>
      </map:serializer>
    </map:serializers>
 
  </map:components>

<!-- ========================== Flowscript ================================= -->

  <map:flow language="javascript">
    <map:script src="fallback://lenya/modules/webdav/webdav.js"/>
  </map:flow>

<!-- =========================== Views ===================================== -->

  <map:views>    
    <map:view name="content" from-label="content">
      <map:serialize type="xml" />
    </map:view>
  </map:views>

  <map:pipelines>
    
    <map:pipeline type="noncaching">

      <map:match pattern="limitResponse.xsl">
        <map:generate type="file" src="cocoon:/request/generate"/>
        <map:transform src="fallback://lenya/modules/webdav/xslt/limitResponse.xsl"/>
        <map:serialize type="xml" status-code="200"/>
      </map:match>
      
      <!-- 
        This internal sitemap fragment renders the usecase, once the
        executeUsecase() flowscript has completed and issued a redirect,
        which is matched here.
        -->
      <map:match pattern="view/*/**" internal-only="true">
        <map:generate type="jx" src="fallback://lenya/modules/webdav/{2}.jx"/>
        <map:transform type="i18n">
          <map:parameter name="locale" value="{request:locale}"/>
        </map:transform>
        <map:transform type="xslt" src="fallback://lenya/modules/webdav/xslt/NSclean.xsl"/>
        <map:select type="request-method">
          <map:when test="PROPFIND">
<!--            <map:transform src="cocoon:/limitResponse.xsl" />-->
            <map:serialize type="xml" status-code="207"/>
          </map:when>
          <map:otherwise>
            <map:serialize type="xml" status-code="200"/>
          </map:otherwise>
        </map:select>
      </map:match>
      
    </map:pipeline>
    
<!-- ========================= Private Resources =========================== -->

    <map:pipeline type="noncaching" internal-only="true">
      
      <map:match pattern="request/generate">
        <map:generate type="stream" />
        <map:transform type="incoming-proxy"/>
        <map:transform type="url2uuid"/>
        <map:serialize type="xml" />
      </map:match>
      
      <!--+ 
          | If you need to check or change the XML that is being PUT
          | you can do that here. This is the pipeline that will
          | generate the input for the PUT method.
          + -->
      <map:match pattern="request/PUT/*">    
        <map:generate type="stream">
          <map:parameter name="defaultContentType" value="text/xml" />
        </map:generate>
        <map:act type="resource-exists" src="fallback://lenya/modules/{page-envelope:document-type}/xslt/clean-{page-envelope:document-type}.xsl">
          <map:transform src="fallback://lenya/modules/{page-envelope:document-type}/xslt/clean-{page-envelope:document-type}.xsl"/>        
          <map:transform src="fallback://lenya/xslt/util/strip_namespaces.xsl"/>
        </map:act>
        <map:transform type="incoming-proxy"/>
        <map:transform type="url2uuid"/>
        <map:serialize type="xml"/>
      </map:match>
      
    </map:pipeline>
    
<!-- ========================= Public Resources =========================== -->
    
    <map:pipeline type="caching">
    
      <!-- Disable caching for webdav/authoring area -->
      <map:act type="set-header">
        <map:parameter name="Cache-Control" value="no-cache" />
      </map:act>    
 
      <map:match pattern="webdav">
        <map:call function="selectMethod">
          <map:parameter name="page" value=""/>
          <map:parameter name="requestURI" value="{request:requestURI}" />
        </map:call>
      </map:match>
      <map:match pattern="webdav/**">
        <map:call function="selectMethod">
          <map:parameter name="page" value="{1}"/>
          <map:parameter name="requestURI" value="{request:requestURI}" />
        </map:call>
      </map:match>
      <map:match pattern="authoring/**">
        <map:call function="selectMethod">
          <map:parameter name="page" value="{1}"/>
          <map:parameter name="requestURI" value="{request:requestURI}" />
        </map:call>
      </map:match>
      <map:match pattern="*/authoring/**">
        <map:call function="selectMethod">
          <map:parameter name="page" value="{1}"/>
          <map:parameter name="requestURI" value="{request:requestURI}" />
        </map:call>
      </map:match>
      

      <map:match pattern="{page-envelope:publication-id}">
        <map:call function="selectMethod">
          <map:parameter name="page" value=""/>
          <map:parameter name="requestURI" value="{request:requestURI}" />
        </map:call>
      </map:match> 
      <map:match pattern="{page-envelope:publication-id}/">
        <map:call function="selectMethod">
          <map:parameter name="page" value=""/>
          <map:parameter name="requestURI" value="{request:requestURI}" />
        </map:call>
      </map:match>        
      
      <map:match pattern="GET/**">
        <map:call function="get">
          <map:parameter name="forward" value="{resource-type:format-webdavGET}"/>
        </map:call>
      </map:match>

      <map:match pattern="PROPFIND/**.*">
<!--
        <map:match pattern="PROPFIND/**_*.html">
          <map:act type="resource-exists" src="site://{page-envelope:publication-id}/authoring/{2}/{1}">
            <map:call function="filePropfind" >
              <map:parameter name="last-modified" value="{page-envelope:document-lastmodified}"/>
            </map:call>
          </map:act>
        </map:match>
-->
        <map:match pattern="PROPFIND/**_*.*">
          <map:act type="resource-exists" src="site://{page-envelope:publication-id}/authoring/{2}/{1}">
            <map:call function="filePropfind" >
              <map:parameter name="last-modified" value="{page-envelope:document-lastmodified}"/>
            </map:call>
          </map:act>
        </map:match>
        <!-- does not exist -->
        <map:generate src="context://lenya/content/util/empty.xml"/>
        <map:transform src="fallback://lenya/xslt/exception/generic.xsl"/>
        <map:transform src="fallback://lenya/xslt/util/page2xhtml.xsl"/>
        <map:transform src="fallback://lenya/xslt/util/strip_namespaces.xsl"/>
        <map:serialize type="xhtml" status-code="404"/>
      </map:match>
 
      <map:match pattern="PROPFIND/**">        
        <map:call function="propfind" /> 
      </map:match>
      
      <map:match pattern="OPTIONS/**">
        <map:call function="options">
          <map:parameter name="uri" value="{request:requestURI}" />
        </map:call>
      </map:match>

      <map:match pattern="HEAD/**.*">
        <map:match pattern="HEAD/**_*.html">
          <map:act type="resource-exists" src="context://lenya/pubs/{page-envelope:publication-id}/content/authoring/{1}/index_{page-envelope:document-language}.xml">
            <map:act type="set-header">
              <map:parameter name="Last-Modified" value="{page-envelope:document-lastmodified}" />
              <map:generate src="context://lenya/content/util/empty.xml"/>
              <map:serialize status-code="200"/>
            </map:act>
          </map:act>
        </map:match>      
        <map:match pattern="HEAD/**_*.*">
          <map:act type="resource-exists" src="context://lenya/pubs/{page-envelope:publication-id}/content/authoring/{1}/index_{page-envelope:document-language}.{3}">
            <map:act type="set-header">
              <map:parameter name="Last-Modified" value="{page-envelope:document-lastmodified}" />
              <map:generate src="context://lenya/content/util/empty.xml"/>
              <map:serialize status-code="200"/>
            </map:act>
          </map:act>
        </map:match>
        <!-- doesn't exist -->
        <map:generate src="context://lenya/content/util/empty.xml"/>
        <map:serialize status-code="404"/>
      </map:match>
      
      <map:match pattern="HEAD/**">
        <map:call function="propfind" />
      </map:match>
      
    </map:pipeline>
    
    <map:pipeline type="noncaching">

      <map:match pattern="checkin">
        <map:act type="reserved-checkin">
          <map:generate src="context://lenya/content/util/empty.xml"/>
          <map:serialize type="xml" status-code="423"/>
        </map:act>
        <map:generate src="context://lenya/content/util/empty.xml"/>
        <map:serialize type="xml" status-code="201" />
      </map:match>

      <map:match pattern="PUT/**">
        <map:call function="put" />  
      </map:match> 
      
      
      <map:match pattern="MKCOL/**">
        <map:call function="mkcol" />
      </map:match>
            
      <map:match pattern="DELETE/**">
        <map:match pattern="DELETE/**_*.html">
          <map:act type="resource-exists" src="context://lenya/pubs/{page-envelope:publication-id}/content/authoring/{1}/index_{page-envelope:document-language}.xml">
            <map:call function="remove" />
          </map:act>
        </map:match>
        <map:match pattern="DELETE/**_*.*">
          <map:act type="resource-exists" src="context://lenya/pubs/{page-envelope:publication-id}/content/authoring/{1}/index_{page-envelope:document-language}.{3}">
            <map:call function="remove" />
          </map:act>
        </map:match>
        <map:match pattern="DELETE/**">
          <map:act type="resource-exists" src="context://lenya/pubs/{page-envelope:publication-id}/content/authoring/{1}">
            <map:call function="remove" />
          </map:act>
        </map:match>
        <!-- does not exist -->
        <map:generate src="context://lenya/content/util/empty.xml"/>
        <map:serialize status-code="200"/>
      </map:match> 
                        
      <map:match pattern="LOCK/**">
        <map:select type="resource-exists">
          <map:parameter name="prefix" value="site://"/>
          <map:when test="{page-envelope:publication-id}/authoring/{page-envelope:document-language}{page-envelope:document-path}">
            <map:act type="reserved-checkout">
              <map:generate src="context://lenya/content/util/empty.xml"/>
              <map:serialize type="xml"  status-code="423"/>
            </map:act>
           <map:act type="set-header">
            <map:parameter name="Lock-Token" value="{page-envelope:document-uuid}"/>
            <map:generate type="file" src="cocoon:/request/generate"/>
            <map:transform type="xslt" src="fallback://lenya/modules/webdav/xslt/lock.xsl">
              <map:parameter name="userid" value="{access-control:user-id}"/>
              <map:parameter name="docid" value="{page-envelope:document-uuid}"/>
            </map:transform>
            <map:serialize type="xml" status-code="200" />
            </map:act> 
          </map:when>
          <map:otherwise>
            <map:generate src="context://lenya/content/util/empty.xml"/>
            <map:serialize type="xml" status-code="412"/>
          </map:otherwise>
        </map:select>
      </map:match>

      <map:match pattern="UNLOCK/**">
        <map:act type="reserved-checkin">
          <map:generate src="context://lenya/content/util/empty.xml"/>
          <map:serialize type="xml"  status-code="424"/>
        </map:act>
        <map:generate src="context://lenya/content/util/empty.xml"/>
        <map:serialize type="xml" status-code="204" />
      </map:match>
      
    </map:pipeline>

  </map:pipelines>

</map:sitemap>
