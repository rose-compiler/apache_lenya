<?xml version="1.0" encoding="UTF-8"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<!-- $Id: emptyTrash.jx 164233 2005-04-22 13:01:45Z jwkaltz $ -->

<page:page
  xmlns:jx="http://apache.org/cocoon/templates/jx/1.0"
  xmlns:page="http://apache.org/cocoon/lenya/cms-page/1.0"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:i18n="http://apache.org/cocoon/i18n/2.1"    
  >
  
  <page:body>
    <jx:set var="style" value="${usecase.getParameter('style')}"/>
    <jx:set var="variables" value="${usecase.getParameter('variables')}"/>
    <div style="float: left;">
      <h2>
        <div style="float: right;">
          <form style="display: inline;">
            <input type="submit" value="back-to-document" i18n:attr="value"/>
          </form>
          
          <form style="display: inline;">
            <input type="hidden" name="lenya.usecase" value="${usecase.getName()}"/>
            <jx:choose>
              <jx:when test="${style.equals('text')}">
                <input type="hidden" name="style" value="columns"/>
                <input type="submit" name="submit" value="state-as-columns" i18n:attr="value"/>
              </jx:when>
              <jx:otherwise>
                <input type="hidden" name="style" value="text"/>
                <input type="submit" name="submit" value="state-as-text" i18n:attr="value"/>
              </jx:otherwise>
            </jx:choose>
          </form>
        </div>
        <i18n:text><jx:out value="${usecase.getName()}"/></i18n:text>
      </h2>
      
      <jx:import uri="fallback://lenya/modules/usecase/templates/messages.jx"/>
      
      <table class="lenya-table-list">
        
        <tr>
          <th><i18n:text>Path</i18n:text></th>
          <th><i18n:text>Language</i18n:text></th>
          <jx:choose>
            <jx:when test="${style.equals('text')}">
              <th><i18n:text>State</i18n:text></th>
            </jx:when>
            <jx:otherwise>
              <jx:forEach var="state" items="${usecase.getParameter('states')}">
                <th><i18n:text><jx:out value="${state}"/></i18n:text></th>
              </jx:forEach>
            </jx:otherwise>
          </jx:choose>
          <jx:forEach var="variable" items="${variables}">
            <th><i18n:text>${variable}</i18n:text></th>
          </jx:forEach>
          <th colspan="${usecase.getParameter('usecases').size()}"><i18n:text>Actions</i18n:text></th>
        </tr>
        
        <jx:forEach var="doc" items="${usecase.getParameter('documents')}">
          <tr>
            <jx:set var="path" value="${doc.getPath()}"/>
            <jx:set var="depth" value="${path.split('/').size() - 2}"/>
            <jx:set var="whitespace" value="&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"/>
            <jx:set var="indent" value="${whitespace.substring(0,  
                                        java.lang.Integer.valueOf(java.lang.Math.min(depth.intValue(),whitespace.length())))}"/>
            <jx:set var="lastSlashIndex" value="${java.lang.Integer.valueOf(path.lastIndexOf('/'))}"/>
            <jx:set var="nodeName" value="${path.substring(lastSlashIndex.intValue())}"/>
            <jx:set var="languages" value="${doc.getLanguages()}"/>
            <jx:if test="${doc.getLanguage().equals(java.util.Arrays.asList(languages).get(0))}">
              <td rowspan="${doc.getLanguages().size()}" style="vertical-align: top">
                <jx:out value="${indent}"/><jx:out value="${indent}"/><jx:out value="${indent}"/><jx:out value="${indent}"/><jx:out value="${nodeName}"/>
              </td>
            </jx:if>
            <td>
              <a href="${doc.getUrl()}"><jx:out value="${doc.getLanguage()}"/></a>
            </td>
            <jx:choose>
              <jx:when test="${style.equals('text')}">
                <td>
                  <i18n:text><jx:out value="${doc.getState()}"/></i18n:text>
                </td>
              </jx:when>
              <jx:otherwise>
                <jx:forEach var="state" items="${usecase.getParameter('states')}">
                  <jx:choose>
                    <jx:when test="${state.equals(doc.getState())}">
                      <td style="text-align: center">x</td>
                    </jx:when>
                    <jx:otherwise>
                      <td/>
                    </jx:otherwise>
                  </jx:choose>
                </jx:forEach>
              </jx:otherwise>
            </jx:choose>
            <jx:forEach var="variable" items="${variables}">
              <td style="text-align: center">
                <jx:choose>
                  <jx:when test="${doc.getValue(variable)}">&#x2713;</jx:when>
                  <jx:otherwise><span style="color: #999999">-</span></jx:otherwise>
                </jx:choose>
              </td>
            </jx:forEach>
            <!--
              <jx:forEach var="usecaseName" items="${doc.getUsecases()}">
              <form action="${usecase.getSourceURL()}" method="POST" id="form-overview" style="display: inline;">
              <input type="hidden" name="lenya.usecase" value="workflow.multiWorkflowInvoke"/>
              <input type="hidden" name="usecaseName" value="${usecaseName}"/>
              <input type="hidden" name="url" value="${doc.getUrl()}"/>
              <input type="submit" name="submit" value="${usecaseName}" i18n:attr="value"/>
              </form>
              </jx:forEach>
            -->
            <jx:set var="usecases" value="${usecase.getParameter('usecases')}"/>
            <jx:forEach var="usecaseName" items="${usecases}">
              <td style="padding-right: 0px; padding-left: 0px;">
                <jx:if test="${doc.canInvoke(usecaseName)}">
                  <form action="${usecase.getSourceURL()}" method="POST" id="form-overview" style="display: inline; padding-right: 4px;">
                    <input type="hidden" name="lenya.usecase" value="workflow.multiWorkflowInvoke"/>
                    <input type="hidden" name="usecaseName" value="${usecaseName}"/>
                    <input type="hidden" name="url" value="${doc.getUrl()}"/>
                    <input type="submit" name="submit" value="${usecaseName}" i18n:attr="value"/>
                  </form>
                </jx:if>
              </td>
            </jx:forEach>
          </tr>
        </jx:forEach>
        
      </table>
    </div>
  </page:body>
</page:page>
