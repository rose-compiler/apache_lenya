<?xml version="1.0" encoding="UTF-8"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<!-- $Id: user.jx 738135 2009-01-27 16:32:15Z andreas $ -->

<page:page
  xmlns:jx="http://apache.org/cocoon/templates/jx/1.0"
  xmlns:page="http://apache.org/cocoon/lenya/cms-page/1.0"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:i18n="http://apache.org/cocoon/i18n/2.1"    
  >
  
  <page:body>
    <jx:import uri="fallback://lenya/modules/usecase/templates/tabs.jx"/>
    <div id="contentblock1" class="lenya-tab">
      <h1><i18n:text>User Profile</i18n:text></h1>
      
      <jx:choose>
        <jx:when test="${usecase.getParameter('user')}">
          <jx:set var="user" value="${usecase.getParameter('user')}"/>
          <table class="lenya-table-noborder">
            <tr>
              <td colspan="2">
                <jx:import uri="fallback://lenya/modules/usecase/templates/messages.jx"/>
              </td>
            </tr>
            <tr>
              <td class="lenya-entry-caption"><i18n:text>User ID</i18n:text>:</td>
              <td><jx:out value="${usecase.getParameter('userId')}"/></td>
            </tr>
            <tr>
              <td class="lenya-entry-caption"><i18n:text>Name</i18n:text>:</td>
              <td><jx:out value="${user.getName()}"/></td>
            </tr>
            <jx:if test="${user.getLdapId()}">
              <tr>
                <td class="lenya-entry-caption"><i18n:text>LDAP ID</i18n:text>:</td>
                <td><jx:out value="${user.getLdapId()}"/></td>
              </tr>
            </jx:if>
            <tr>
              <td valign="top" class="lenya-entry-caption"><i18n:text>Description</i18n:text>:</td>
              <td>
                <jx:out value="${user.getDescription()}"/>
              </td>
            </tr>
            
            <tr>
              <td class="lenya-entry-caption"><i18n:text>E-Mail</i18n:text>:</td>
              <td><jx:out value="${user.getEmail()}"/></td>
            </tr>
            
            <tr>
              <td class="lenya-entry-caption"><i18n:text>Default Menu Locale</i18n:text>:</td>
              <td><jx:out value="${user.getDefaultMenuLocale()}"/></td>
            </tr>
            
            <tr>
              <td class="lenya-entry-caption"><i18n:text>Default Document Locale</i18n:text>:</td>
              <td><jx:out value="${user.getDefaultDocumentLocale()}"/></td>
            </tr>
            
            <tr>
              <td/>
              <td>
                <form id="form-edit_user_profile" action="${usecase.getSourceURL()}">
                  <input type="hidden" name="lenya.usecase" value="admin.userProfile"/>
                  <input type="hidden" name="userId" value="${user.getId()}"/>
                  <input i18n:attr="value" type="submit" value="Edit Profile" name="input-edit_user_profile"/>
                </form>
              </td>
            </tr>
            
            <jx:if test="${user.canChangePassword() == 'true'}"> 
              <tr>
                <td/>
                <td>
                  <form id="form-change_user_password" action="${usecase.getSourceURL()}">
                    <input type="hidden" name="lenya.usecase" value="admin.changePasswordAdmin"/>
                    <input type="hidden" name="userId" value="${user.getId()}"/>
                    <input i18n:attr="value" type="submit" value="Change Password" name="input-change_user_password"/>
                  </form>
                </td>
              </tr>
            </jx:if>
            
            <tr>
              <td class="lenya-entry-caption"><i18n:text>Groups</i18n:text>:</td>
              <td>
                <jx:set var="groups" value="${user.getGroups()}"/>
                <jx:set var="groupIds" value="${java.util.TreeSet()}"/>
                <jx:forEach var="group" items="${groups}">
                  <jx:set var="ignore" value="${groupIds.add(group.getId())}"/>
                </jx:forEach>
                <jx:choose>
                  <jx:when test="${groupIds.size() == 0}">
                    <i18n:text>No groups assigned</i18n:text>
                  </jx:when>
                  <jx:otherwise>
                    <jx:forEach var="group" items="${groupIds}"><jx:if test="${group != groupIds.first()}">, </jx:if>
                      <a href="${request.pathInfo}?lenya.usecase=admin.group&amp;groupId=${group}"><jx:out value="${group}"/></a></jx:forEach>
                  </jx:otherwise>
                </jx:choose>
              </td>
            </tr>
            
            <tr>
              <td/>
              <td>
                <form id="form-edit_group_affiliation" action="${usecase.getSourceURL()}">
                  <input type="hidden" name="lenya.usecase" value="admin.userGroups"/>
                  <input type="hidden" name="userId" value="${user.getId()}"/>
                  <input i18n:attr="value" type="submit" value="Edit Group Affiliation" name="input-edit_group_affiliation"/>
                </form>
              </td>
            </tr>
            
            <tr>
              <td/>
              <td>
                <form id="form-user_profile_cancel" action="${usecase.getSourceURL()}">
                  <input type="hidden" name="lenya.continuation" value="${continuation.id}"/>
                  <input type="hidden" name="lenya.usecase" value="${request.getParameter('lenya.usecase')}"/>
                  <input i18n:attr="value" type="submit" name="cancel" value="Cancel"/>
                </form>
              </td>
            </tr>
          </table>
        </jx:when>
        <jx:otherwise>
          <p>
            The parameter 'user' is missing. Please go back to the
            <a href="?lenya.usecase=admin.users">users overview page</a>
            and click on a user link.
          </p>
        </jx:otherwise>
      </jx:choose>
      
    </div>
  </page:body>
</page:page>
